VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "計算"
   ClientHeight    =   1380
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   1380
   ScaleWidth      =   6015
   StartUpPosition =   2  '画面の中央
   Begin VB.TextBox Text2 
      BeginProperty Font 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   12
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   2
      Text            =   "結果"
      Top             =   840
      Width           =   4575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "計算"
      Default         =   -1  'True
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   1095
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   12
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   5775
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "式を入力  [ 0〜9 . + - * / ( ) ]"
      Height          =   180
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   2340
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Function StrDelSpace(str1 As String) As String
    Dim str2 As String, str3 As String
    Dim i As Integer
    
    str2 = ""
    For i = 1 To Len(str1)
        str3 = Mid$(str1, i, 1)
        If str3 <> " " Then
            str2 = str2 & str3
        End If
    Next i
    
    StrDelSpace = str2
    
End Function

Private Function StrSep(str1 As String) As String()
    Dim str2 As String
    Dim i As Integer, n As Integer, f As Integer
    Dim s() As String
    
    ReDim s(0)
    
    f = 0
    n = 0
    For i = 1 To Len(str1)
        str2 = Mid$(str1, i, 1)
        If InStr(1, "+*/=", str2) > 0 Then
            If (f <> 1) And (f <> 5) And (f <> 6) Then
                ReDim Preserve s(n)
                s(n) = str2
                n = n + 1
            Else
                s(n - 1) = s(n - 1) & str2
            End If
            f = 1
        ElseIf str2 = "-" Then
            If (f = 2) Or (f = 4) Or (f = 7) Or (f = 8) Then
                ReDim Preserve s(n)
                s(n) = str2
                n = n + 1
                f = 1
            ElseIf (f = 0) Or (f = 1) Or (f = 3) Then
                ReDim Preserve s(n)
                s(n) = str2
                n = n + 1
                f = 5
            Else
                s(n - 1) = s(n - 1) & str2
                f = 6
            End If
        ElseIf str2 = "(" Then
            ReDim Preserve s(n)
            s(n) = str2
            n = n + 1
            f = 3
        ElseIf str2 = ")" Then
            ReDim Preserve s(n)
            s(n) = str2
            n = n + 1
            f = 4
        ElseIf str2 = "." Then
            If (f = 2) Or (f = 5) Then
                s(n - 1) = s(n - 1) & str2
                f = 7
            ElseIf (f = 7) Or (f = 8) Then
                ReDim Preserve s(n)
                s(n) = str2
                n = n + 1
                f = 8
            Else
                ReDim Preserve s(n)
                s(n) = str2
                n = n + 1
                f = 7
            End If
        Else
            If (f <> 2) And (f <> 5) And (f <> 7) Then
                ReDim Preserve s(n)
                s(n) = str2
                n = n + 1
                f = 2
            Else
                s(n - 1) = s(n - 1) & str2
                If f <> 7 Then
                    f = 2
                End If
            End If
            
        End If
    Next i
    
    StrSep = s
    
End Function

Private Function Keisan(num1 As String, num2 As String, exp1 As String) As String
    On Error GoTo Keisan_Error
    Select Case exp1
    Case "+"
        Keisan = CStr(Val(num1) + Val(num2))
    Case "-"
        Keisan = CStr(Val(num1) - Val(num2))
    Case "*"
        Keisan = CStr(Val(num1) * Val(num2))
    Case "/"
        If Val(num2) = 0# Then
            Keisan = "EZ"
            Exit Function
        End If
        Keisan = CStr(Val(num1) / Val(num2))
    Case Else
        Keisan = "ER"
    End Select
    Exit Function
Keisan_Error:
    Keisan = "EO"
End Function

Private Sub mes(str1 As String)
    Text2.Text = str1
    Debug.Print str1
End Sub

Private Sub Command1_Click()
    Dim str1 As String, str2 As String
    Dim i As Integer, br As Integer, eq As Integer
    Dim s() As String
    Dim f() As String, g() As String
    Dim fn As Integer, gn As Integer
    
    Debug.Print "------------------------------------------"
    
    str1 = Text1.Text
    Debug.Print "str1 = "; str1
    
    str1 = Trim$(str1)
    Debug.Print "str1 = "; str1
    
    If Len(str1) = 0 Then
        mes "文字が入力されていません"
        Exit Sub
    End If
            
    str1 = StrDelSpace(str1)
    Debug.Print "str1 = "; str1
    
    If Right$(str1, 1) <> "=" Then
        str1 = str1 & "="
        Debug.Print "str1 = "; str1
    End If
    
    br = 0
    eq = 0
    For i = 1 To Len(str1)
        str2 = Mid$(str1, i, 1)
        If InStr(1, "1234567890.+-*/=()", str2) < 1 Then
            mes "不正な文字が含まれています"
            Exit Sub
        End If
        If str2 = "=" Then
            eq = eq + 1
            If eq > 1 Then
                mes "イコールの使い方が不正です"
                Exit Sub
            End If
        ElseIf str2 = "(" Then
            br = br + 1
        ElseIf str2 = ")" Then
            If br = 0 Then
                mes "カッコの使い方が不正です"
                Exit Sub
            End If
            br = br - 1
        End If
    Next i
    If br > 0 Then
        mes "カッコの使い方が不正です"
        Exit Sub
    End If
    
    If InStr(1, str1, "()") > 0 Then
        mes "カッコの使い方が不正です"
        Exit Sub
    End If
    
    s = StrSep(str1)
    
    Debug.Print "s() = "; Join(s, "|")
    
    For i = 0 To UBound(s)
        str2 = Left$(s(i), 1)
        If InStr(1, "+-*/=", str2) > 0 Then
            If Len(s(i)) > 1 Then
                If IsNumeric(s(i)) = False Then
                    mes "演算子の使い方が不正です"
                    Exit Sub
                End If
            Else
                If i = 0 Then
                    mes "演算子の使い方が不正です"
                    Exit Sub
                End If
            End If
        End If
        If s(i) = "." Then
            mes "小数点の使い方が不正です"
            Exit Sub
        End If
    Next i
    
    Debug.Print "**計算開始**"
    
    fn = 0
    gn = 0
    For i = 0 To UBound(s)
        If IsNumeric(s(i)) Then
            ReDim Preserve f(fn)
            f(fn) = s(i)
            fn = fn + 1
            Debug.Print "Num "; Join(f, " ")
        Else
            ReDim Preserve g(gn)
            g(gn) = s(i)
            gn = gn + 1
            Debug.Print "Exp "; Join(g, " ")
            If gn > 1 Then
                Select Case s(i)
                Case "+"
                    Do
                        If gn < 2 Then
                            Exit Do
                        ElseIf InStr(1, "+-*/", g(gn - 2)) > 0 Then
                            f(fn - 2) = Keisan(f(fn - 2), f(fn - 1), g(gn - 2))
                            If Left$(f(fn - 2), 1) = "E" Then
                                Select Case Right$(f(fn - 2), 1)
                                Case "Z"
                                    mes "ゼロで除算しました"
                                Case "O"
                                    mes "オーバーフローしました"
                                Case Else
                                    mes "予想外のエラーです"
                                End Select
                                Exit Sub
                            End If
                            fn = fn - 1: ReDim Preserve f(fn - 1)
                            g(gn - 2) = s(i)
                            gn = gn - 1: ReDim Preserve g(gn - 1)
                            Debug.Print "-Num "; Join(f, " ")
                            Debug.Print "-Exp "; Join(g, " ")
                        Else
                            Exit Do
                        End If
                    Loop
                Case "-"
                    Do
                        If gn < 2 Then
                            Exit Do
                        ElseIf InStr(1, "-*/", g(gn - 2)) > 0 Then
                            f(fn - 2) = Keisan(f(fn - 2), f(fn - 1), g(gn - 2))
                            If Left$(f(fn - 2), 1) = "E" Then
                                Select Case Right$(f(fn - 2), 1)
                                Case "Z"
                                    mes "ゼロで除算しました"
                                Case "O"
                                    mes "オーバーフローしました"
                                Case Else
                                    mes "予想外のエラーです"
                                End Select
                                Exit Sub
                            End If
                            fn = fn - 1: ReDim Preserve f(fn - 1)
                            g(gn - 2) = s(i)
                            gn = gn - 1: ReDim Preserve g(gn - 1)
                            Debug.Print "-Num "; Join(f, " ")
                            Debug.Print "-Exp "; Join(g, " ")
                        Else
                            Exit Do
                        End If
                    Loop
                Case "*", "/"
                    Do
                        If gn < 2 Then
                            Exit Do
                        ElseIf InStr(1, "*/", g(gn - 2)) > 0 Then
                            f(fn - 2) = Keisan(f(fn - 2), f(fn - 1), g(gn - 2))
                            If Left$(f(fn - 2), 1) = "E" Then
                                Select Case Right$(f(fn - 2), 1)
                                Case "Z"
                                    mes "ゼロで除算しました"
                                Case "O"
                                    mes "オーバーフローしました"
                                Case Else
                                    mes "予想外のエラーです"
                                End Select
                                Exit Sub
                            End If
                            fn = fn - 1: ReDim Preserve f(fn - 1)
                            g(gn - 2) = s(i)
                            gn = gn - 1: ReDim Preserve g(gn - 1)
                            Debug.Print "-Num "; Join(f, " ")
                            Debug.Print "-Exp "; Join(g, " ")
                        Else
                            Exit Do
                        End If
                    Loop
'                Case "/"
'                    Do
'                        If gn < 2 Then
'                            Exit Do
'                        ElseIf g(gn - 2) = "/" Then
'                            f(fn - 2) = Keisan(f(fn - 2), f(fn - 1), g(gn - 2))
'                            fn = fn - 1: ReDim Preserve f(fn - 1)
'                            g(gn - 2) = s(i)
'                            gn = gn - 1: ReDim Preserve g(gn - 1)
'                            Debug.Print "-Num "; Join(f, " ")
'                            Debug.Print "-Exp "; Join(g, " ")
'                        Else
'                            Exit Do
'                        End If
'                    Loop
                Case "="
                    Do
                        If gn > 1 Then
                            f(fn - 2) = Keisan(f(fn - 2), f(fn - 1), g(gn - 2))
                            If Left$(f(fn - 2), 1) = "E" Then
                                Select Case Right$(f(fn - 2), 1)
                                Case "Z"
                                    mes "ゼロで除算しました"
                                Case "O"
                                    mes "オーバーフローしました"
                                Case Else
                                    mes "予想外のエラーです"
                                End Select
                                Exit Sub
                            End If
                            fn = fn - 1: ReDim Preserve f(fn - 1)
                            gn = gn - 1: ReDim Preserve g(gn - 1)
                            Debug.Print "-Num "; Join(f, " ")
                            Debug.Print "-Exp "; Join(g, " ")
                        Else
                            Exit Do
                        End If
                    Loop
                Case "("
                Case ")"
                    Do
                        If g(gn - 2) = "(" Then
                            gn = gn - 1: ReDim Preserve g(gn - 1)
                            gn = gn - 1
                            If gn = 0 Then
                                Erase g
                            Else
                                ReDim Preserve g(gn - 1)
                            End If
                            Exit Do
                        Else
                            f(fn - 2) = Keisan(f(fn - 2), f(fn - 1), g(gn - 2))
                            If Left$(f(fn - 2), 1) = "E" Then
                                Select Case Right$(f(fn - 2), 1)
                                Case "Z"
                                    mes "ゼロで除算しました"
                                Case "O"
                                    mes "オーバーフローしました"
                                Case Else
                                    mes "予想外のエラーです"
                                End Select
                                Exit Sub
                            End If
                            fn = fn - 1: ReDim Preserve f(fn - 1)
                            gn = gn - 1: ReDim Preserve g(gn - 1)
                            Debug.Print "-Num "; Join(f, " ")
                            Debug.Print "-Exp "; Join(g, " ")
                        End If
                    Loop
                End Select
            End If
        End If
    Next i
    
    mes "結果は  " & f(0) & "  です"
    
End Sub

Private Sub Form_Load()

End Sub
